import numpy as np
import random
import pandas as pd
from .statistic import ChiSquare

class SVM:
    def __init__(self, learning_rate = 0.001, alpha = 0.01, t_end = 10):
        self._lr = learning_rate
        self._m_c = self._m_p = 0
        self._sv = {1: 0, -1: 0}
        self._alpha = alpha
        self._t_end = t_end
        self.weights = None
        self._eta = None
        self._fac = None

    def _init_iter(self, t):
        self._m_p = self._m_c
        self._sv = {1: 0, -1: 0}
        self._eta = 1/(self._lr*t)
        self._fac = (1 - (self._eta * self._lr))*self.weights
        
    def train(self, x, y):
        x_train = np.c_[x, np.ones(x.shape[0])]
        x_dim = x_train.shape[1]

        converged = False
        t = 0
        order = np.arange(0, x_train.shape[0], 1)
        
        self.weights = np.zeros(x_dim)

        while not converged:
            t += 1
            self._init_iter(t)
            random.shuffle(order)
            for i in  order:
                y_pred = x_train[i].dot(self.weights)
                prediction = round(y_pred, 1)
                if prediction in self._sv:
                    self._sv[prediction] += 1

                self.weights = self._fac + (
                    self._eta * y[i] * x_train[i] if y[i] * y_pred < 1 else 0
                )

            if t > self._t_end:
                self._m_c = np.linalg.norm(self.weights)
                if (self._sv[1] > 0 and self._sv[-1] > 0) and (self._m_c - self._m_p) < self._alpha:
                    converged = True

    def predict(self, x):
        x_train = np.c_[x, np.ones(x.shape[0])]
        return np.where(x_train.dot(self.weights) > 1, 1, -1)


class NVDistribution:
    def __init__(self):
        self.cond_features = {}
        self.classes = {}
        self.records = 0

    def _get_per_class_prob(self, cls, feature_vector):
        return (self.classes[cls]/self.records) * np.prod(
            [self.cond_features[i].get(feature_vector[i], {}).get(cls, 0)/self.classes[cls] for i in range(len(feature_vector))]
        )

    def get_class_prob(self, feature_vector):
        return [
            {'class': cls, 'class_prob': self._get_per_class_prob(cls, feature_vector)} for cls in self.classes
        ]
    
class NaiveBayes:
    def __init__(self):
        self.dist = None
    
    def create_classes(self, x, y):
        for feature in range(x.shape[1]):
            class_vector = {}
            feature_vector = x[:,  feature]
            counts = {}
            for i in range(len(y)):
                fv_class = class_vector.get(feature_vector[i], {})
                counts[feature_vector[i]] = counts.get(feature_vector[i], 0) + 1
                fv_class[y[i]] = fv_class.get(y[i], 0) + 1
                class_vector[feature_vector[i]] = fv_class
               
            self.dist.cond_features[feature] = class_vector

        for i in range(y.shape[0]):
            self.dist.classes[y[i]] = self.dist.classes.get(y[i], 0) + 1

        self.dist.records = len(y)
   
    #(x1, x2, x3), (y)
    def train(self, x, y):
        self.dist = NVDistribution()
        self.create_classes(x, y)
    
    def predict(self, x):
        nv_prob = self.dist.get_class_prob(x)

        return nv_prob[np.argmax([r['class_prob'] for r in nv_prob])]['class']

class CHAID:
    def __init__(self):
        self.tree = []
        self.chi_square = None

    def __build_tree(self, data_frame, feature_columns, decision_column):
        if len(data_frame[decision_column].unique()) == 1:
            return data_frame[decision_column].unique()[0]
        
        df = pd.DataFrame.copy(data_frame)
        chsq = {
            feature_column: self.chi_square(df[[feature_column, decision_column]], feature_column)
            for feature_column in feature_columns
        }

        max_chsq_feature = max(chsq, key=lambda key: chsq[key])
        remain_features = [feature for feature in feature_columns if feature != max_chsq_feature]
        return {'key': max_chsq_feature, 'next': {
            feature_value: self.__build_tree(
                df[df[max_chsq_feature] == feature_value],
                remain_features,
                decision_column
            )
            for feature_value in df[max_chsq_feature].unique()
        }}
        


    def train(self, data_frame, feature_columns, decision_column):
        decision_classes = data_frame[decision_column].unique()
        class_p = {cls: 1/len(decision_classes) for cls in decision_classes}
        self.chi_square = ChiSquare(class_p)
        self.tree = self.__build_tree(data_frame, feature_columns, decision_column)

    def test(self, series):
        traversal_tree = self.tree
        while type(traversal_tree) == dict:
            key = traversal_tree['key']
            traversal_tree = traversal_tree['next'][series[key]]

        return traversal_tree

            
