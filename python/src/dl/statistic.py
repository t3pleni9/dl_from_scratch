from typing import Dict
import pandas as pd
import numpy as np
class GradientDescent:
    def __init__(self, learning_rate = 0.001):
        self._lr = learning_rate
        
    def __call__(self, var, df):
        return var - self._lr * df()


class MeanSquareError:
    def __call__(self, y, y_hat):
        return np.mean((y - y_hat) ** 2)


class ChiSquare:
    def __init__(self, p_dist: Dict[str, float]):
        self.p_dist = p_dist

    def __call__(self, features: pd.DataFrame, feature_column: str) -> float:
        features = pd.DataFrame.copy(features)
        if len(features.columns) != 2:
            raise Exception(f"Expected: <Feature>, <Dependent>. Found {', '.join(features.columns)}")

        class_column = [col for col in features.columns if col != feature_column][0]
        features['__count'] = 1
        feature_pivot = features.pivot_table(
            index=[feature_column],
            columns=[class_column],
            aggfunc=np.sum,
            fill_value=0
        )['__count']
        class_p = [{cls: np.sum(feature_pivot.loc[index])*self.p_dist[cls] for cls in self.p_dist} for index in feature_pivot.index]
        
        chi_square = 0
        for cls in self.p_dist:
            feature_pivot[f'{cls}_expected'] = [expected[cls] for expected in class_p]
            feature_pivot[f'{cls}_deviation'] = feature_pivot[cls] - feature_pivot[f'{cls}_expected']
            feature_pivot[f'{cls}_chi_sqr'] = np.sqrt(
                pow(feature_pivot[f'{cls}_deviation'], 2)/feature_pivot[f'{cls}_expected']
            )

            chi_square += np.sum(feature_pivot[f'{cls}_chi_sqr'])

        return chi_square
