import numpy as np
from .statistic import GradientDescent, MeanSquareError
class LinearRegression:
    '''
    Regression y = B0 + x*B1
    X coefficient B1 = sum((x_i - mean(x)) * (y_i - mean(y)))/sum((x_i - mean(x))^2)
    Intercept B0 = mean(y) - B1*mean(x)
    '''
    def __init__(self):
        self.b0 = None
        self.b1 = None

    def train(self, x, y):
        self.b1 = np.sum((x - np.mean(x)) * (y - np.mean(y)))/np.sum((x - np.mean(x)) ** 2)
        self.b0 = np.mean(y) - self.b1 * np.mean(x)

    def predict(self, x):
        return self.b0 + self.b1 * x


class MultipleLinearRegression:
    '''
    With gradient descent
    y = wx + b
    MSE = mean((y_hat - y)^2)
    dw = 1/n sum(2 * x (y_hat - y)) 
    db = 1/n sum(2 * (y_hat - y)) 

    '''
    def __init__(self, learning_rate=0.001, n_iter = 1000):
        self.bias = None
        self.weights = None
        self._n_iter = n_iter
        self._gd = GradientDescent(learning_rate = learning_rate)
        self._mse = MeanSquareError()
        self.loss = []
        
    def train(self, x, y):
        x_dim = x.shape[1]
        self.weights = np.zeros(x_dim)
        self.bias = 0

        for i in range(self._n_iter):
            y_hat = (x.dot(self.weights) + self.bias)
            loss = self._mse(y, y_hat)
            self.loss.append(loss)
            
            dw = lambda : (1/x.shape[0]) * (2 * x.T.dot(y_hat - y))
            db = lambda : (1/x.shape[0]) * (2 * np.sum(y_hat - y))

            self.weights = self._gd(self.weights, dw)
            self.bias = self._gd(self.bias, db)
            
            
    def predict(self, x):
        x_test = x.reshape(-1, 1) if len(x.shape) == 1 else x
        return x_test.dot(self.weights) + self.bias


